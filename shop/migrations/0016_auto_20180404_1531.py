# Generated by Django 2.0.3 on 2018-04-04 15:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0015_auto_20180404_1527'),
    ]

    operations = [
        migrations.AlterField(
            model_name='currency',
            name='currency_exchange',
            field=models.DecimalField(decimal_places=0, default=25, max_digits=8),
        ),
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=8),
        ),
    ]
