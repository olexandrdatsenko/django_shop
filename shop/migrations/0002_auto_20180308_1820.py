# Generated by Django 2.0.2 on 2018-03-08 18:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('currency_exchange', models.IntegerField(default=25, verbose_name=10)),
            ],
        ),
        migrations.AddField(
            model_name='product',
            name='meta_tags',
            field=models.CharField(db_index=True, default='phone, mobile appliances', max_length=300),
        ),
        migrations.AlterField(
            model_name='product',
            name='title',
            field=models.CharField(db_index=True, max_length=200),
        ),
        migrations.AlterField(
            model_name='subcategory',
            name='title',
            field=models.CharField(db_index=True, max_length=200),
        ),
    ]
