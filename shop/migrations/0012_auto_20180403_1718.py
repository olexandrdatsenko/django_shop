# Generated by Django 2.0.3 on 2018-04-03 17:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0011_auto_20180402_1530'),
    ]

    operations = [
        migrations.AlterField(
            model_name='currency',
            name='currency_exchange',
            field=models.DecimalField(decimal_places=2, default=25, max_digits=8),
        ),
        migrations.AlterField(
            model_name='product',
            name='meta_description',
            field=models.CharField(db_index=True, default=None, max_length=150),
        ),
        migrations.AlterField(
            model_name='product',
            name='meta_keywords',
            field=models.CharField(db_index=True, default=None, max_length=100),
        ),
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=8),
        ),
        migrations.AlterField(
            model_name='product',
            name='sku',
            field=models.CharField(default='200000', max_length=10),
        ),
    ]
