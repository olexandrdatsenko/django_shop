from shop.models import Category


def menu_context(request):
    # return {'menu_categories': Category.objects.all()}
    return {'menu_categories': Category.objects.prefetch_related('subcategories')}
    
