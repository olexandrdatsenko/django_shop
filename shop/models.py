from django.contrib.auth.models import User
from django.db import models

from base import settings


class Category(models.Model):
    title = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.title


class Subcategory(models.Model):
    category = models.ManyToManyField(Category, related_name="subcategories")
    title = models.CharField(max_length=200, db_index=True)

    class Meta:
        verbose_name_plural = "Subcategories"

    def __str__(self):
        return self.title


class Currency(models.Model):
    name_currency = models.CharField(max_length=10, default="USD")
    UAH = models.CharField(max_length=3, default="UAH")
    currency_exchange = models.DecimalField(max_digits=8, decimal_places=0, default=25)

    class Meta:
        verbose_name_plural = "Currencies"

    def __str__(self):
        return self.name_currency


class Product(models.Model):
    subcategory = models.ManyToManyField(Subcategory)
    currency_pk = models.ForeignKey(Currency, default=1, on_delete=models.CASCADE)
    title = models.CharField(max_length=200, db_index=True)
    description = models.TextField()
    amount = models.IntegerField(default=20)
    price = models.DecimalField(max_digits=8, decimal_places=2, default=0.0)
    image_url = models.CharField(
        max_length=100, default="https://s3.amazonaws.com/django-shop-stuff/"
    )
    on_the_main = models.BooleanField(default=False)
    sku = models.CharField(max_length=10, default="200000")
    meta_keywords = models.CharField(max_length=100, db_index=True, blank=True)
    meta_description = models.CharField(max_length=1000, db_index=True, blank=True)

    __text_desription = """{} - 
     купить на django-shop-test.herokuapp.com: 
    050 123 45 67. Оперативная доставка 
    Гарантия качества
    Лучшая цена $ """

    def save(self, *args, **kwargs):
        if not self.meta_keywords or not self.meta_description:
            self.meta_keywords = self.title
            self.meta_description = self.__text_desription.format(self.title)
        super(Product, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    def get_total_price_in_ua(self):
        return self.price * self.currency_pk.currency_exchange


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    product_fk = models.ManyToManyField(Product)
    name_user = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=20)
    email_user = models.EmailField(max_length=100)
    total_price = models.DecimalField(default=0.0, max_digits=8, decimal_places=2)
    total_count = models.IntegerField()
    order_date = models.DateTimeField(auto_now_add=True)
    product_ids = models.CharField(max_length=200)

    def __str__(self):
        return self.name_user
