from base.settings import EMAIL_HOST_USER
from django.shortcuts import render, get_object_or_404, HttpResponseRedirect
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView, DetailView, View

from .forms import OrderForm
from .for_email import answer_email
from .helpers import (
    products_total_price_amount,
    remove_amount_products_from_db,
    remove_all_products_from_session,
)
from .models import Product, Subcategory, Order
from .tasks import send_message_on_email


class IndexListView(ListView):
    queryset = Product.objects.filter(on_the_main=True).order_by("id")
    context_object_name = "products"
    template_name = "shop/index.html"
    paginate_by = 3

    def get_context_data(self, **kwargs):
        page = self.request.GET.get("page", 1)
        paginator = Paginator(self.queryset, self.paginate_by)
        context = {}
        try:
            products_on_page = paginator.page(page)
            context = {
                "subcategory": products_on_page,
                "products": self.request.session.get("products", []),
                "amount_each": self.request.session.get("amount", {}),
            }
        except PageNotAnInteger:
            context["subcategory"] = paginator.page(1)
        except EmptyPage:
            context["subcategory"] = paginator.page(paginator.num_pages)

        return context


class SubcategoryProductListView(ListView):
    def get(self, request, pk_subcategory):
        products_in_subcategory = (
            Product.objects.select_related("currency_pk")
            .filter(subcategory__in=pk_subcategory)
            .order_by("id")
        )
        page = request.GET.get("page", 1)
        paginator = Paginator(products_in_subcategory, 6)
        context = {}
        # UAH = products_in_subcategory.select_related('currency_pk').filter(UAH='UAH')
        try:
            products_on_page = paginator.page(page)
            context = {
                "subcategory": products_on_page,
                "subcategory_name": get_object_or_404(Subcategory, id=pk_subcategory),
                "products": request.session.get("products", []),
                "amount_each": request.session.get("amount", {}),
            }
        except PageNotAnInteger:
            context["subcategory"] = paginator.page(1)
        except EmptyPage:
            context["subcategory"] = paginator.page(paginator.num_pages)

        return render(request, "shop/subcategory-product.html", context)


class ProductDetailsView(DetailView):
    model = Product
    context_object_name = "product"
    pk_url_kwarg = "pk_product"
    template_name = "shop/product-details.html"


class AddProductToSessionView(View):
    def get(self, request):
        next_page = request.POST.get("next", "/")
        return HttpResponseRedirect(next_page)

    def post(self, request):
        request.session.modified = True
        if "products" not in request.session:
            request.session["products"] = []
        if "amount" not in request.session:
            request.session["amount"] = {}
        next_page = request.POST.get("next", "/cart")
        pk_product = request.POST.get("product_id")
        request.session["products"].append(int(pk_product))
        if pk_product not in request.session["amount"].keys():
            request.session["amount"].update({pk_product: 1})
        else:
            for pk_in_dict, value in request.session["amount"].items():
                if pk_product == pk_in_dict:
                    first_item = Product.objects.values_list("amount").get(
                        id=pk_product
                    )[0]
                    if 20 > value <= first_item:
                        request.session["amount"].update({pk_product: value + 1})
                    elif value > first_item:
                        messages.info(request, "В наличии нету товаров")
                    else:
                        messages.info(request, "Limit - 20 items")
        return HttpResponseRedirect(next_page)


class RemoveProductFromSessionView(View):
    def get(self, request):
        next_page = request.POST.get("next", "/")
        return HttpResponseRedirect(next_page)

    def post(self, request):
        request.session.modified = True
        next_page = request.POST.get("next", "/cart")
        pk_product = request.POST.get("product_id")
        request.session["products"].remove(int(pk_product))
        quantity = request.session["amount"].get(pk_product)
        request.session["amount"].update({pk_product: quantity - 1})
        if request.session["amount"].get(pk_product) == 0:
            request.session["amount"].pop(pk_product)
        return HttpResponseRedirect(next_page)


class RemoveAllProductsFromSession(View):
    def get(self, request):
        return HttpResponseRedirect(request.POST.get("next", "/"))

    def post(self, request):
        request.session.modified = True
        if request.session.get("products", []):
            request.session["products"].clear()
            request.session["amount"].clear()
        return HttpResponseRedirect(request.POST.get("next", "/cart"))


class CartView(View):
    def get(self, request):
        if request.session.get("products"):
            products, total_price, amount = products_total_price_amount(request)
            amount_all = request.session.get("amount", {})
            context = {
                "products": products,
                "total_price": total_price,
                "amount_all": amount_all,
            }
            return render(request, "shop/cart.html", context)
        else:
            return render(request, "shop/cart.html")


class MakeOrderView(View):

    template_name = "shop/order.html"

    def get(self, request):
        form = OrderForm()
        return render(
            request,
            self.template_name,
            {"form": form, "products": request.session.get("products", [])},
        )

    def post(self, request):
        form = OrderForm(request.POST)
        if form.is_valid():
            name = request.POST.get("name_user")
            phone_number = request.POST.get("phone_number")
            email = request.POST.get("email_user")
            products, total_price, amount = products_total_price_amount(request)
            products = [product.id for product in products]
            remove_amount_products_from_db(request)
            if request.user.is_authenticated:
                form.save(commit=False)
                form.instance.user = request.user
            form.instance.product_ids = str(products)
            form.instance.total_price = total_price
            form.instance.total_count = amount
            form.save()
            message, html = answer_email(request, name, phone_number, total_price)
            send_message_on_email.delay(
                subject="Your order",
                message=message,
                from_email=EMAIL_HOST_USER,
                recipient_list=[email],
                html_message=html,
            )
            messages.success(request, "We'll call you!")
            remove_all_products_from_session(request)
            return HttpResponseRedirect("/")
