from django.core import serializers
from django.http import HttpResponse

from .models import Product


def api(request):
    products = Product.objects.all()
    json_data = serializers.serialize('json', products)
    return HttpResponse(json_data, content_type="application/json")
