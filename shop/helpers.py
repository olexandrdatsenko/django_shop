from .models import Product


def products_total_price_amount(request):
    session_ids = request.session["products"]
    products = Product.objects.filter(id__in=session_ids)
    total_price, amount = 0, 0
    for i in session_ids:
        product = Product.objects.get(id=int(i))
        total_price += product.get_total_price_in_ua()
        amount += 1

    return products, total_price, amount


def remove_amount_products_from_db(request):
    p = Product.objects.values_list("amount")
    amount_products = request.session["amount"]
    for pk in amount_products.keys():
        Product.objects.filter(id=pk).update(
            amount=p.get(id=pk)[0] - amount_products[pk]
        )

def remove_all_products_from_session(request):
    request.session.modified = True
    if request.session.get("products", []):
        request.session["products"].clear()
        request.session["amount"].clear()