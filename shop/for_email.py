from django.template.loader import render_to_string
from .models import Product


def answer_email(request, name, phone_number, total_price):
    session_ids = request.session["products"]
    amount_all = request.session.get("amount", {})
    products = Product.objects.filter(id__in=session_ids)
    context = {
        "name": name,
        "phone_number": phone_number,
        "total_price": total_price,
        "products": products,
        "amount_all": amount_all,
    }

    message = render_to_string("shop/for_email.txt", context)
    html = render_to_string("shop/for_email.html", context)
    return message, html
