import logging

from django.http import HttpResponseRedirect

from shop.models import Product
import requests
from decimal import Decimal

PATH_LOGS = "crontab.log"


def parse_products():
    logging.basicConfig(
        filename=PATH_LOGS,
        level=logging.INFO,
        format="%(asctime)s:%(levelname)s:%(message)s",
    )
    logging.info("__CRON__")
    try:
        url = "https://morning-tundra-15096.herokuapp.com/products.json"
        response = requests.get(url, timeout=15).json()
        logging.info("__CRON__", response)
        img_url = "https://morning-tundra-15096.herokuapp.com/media/"
        for item in response:
            if item["fields"]["sku"] not in Product.objects.values_list(
                "sku", flat=True
            ):  # without flat i get (0,) tuple, not a value

                q = Product(
                    title=item["fields"]["title"],
                    description=item["fields"]["description"],
                    price=Decimal(item["fields"]["price"]),
                    amount=item["fields"]["quantity"],
                    sku=item["fields"]["sku"],
                    image_url=img_url + item["fields"]["image"],
                    meta_keywords=item["fields"]["meta_keywords"],
                    meta_description=item["fields"]["meta_description"],
                )
                logging.info(item["fields"])
                q.save()
    except Exception as e:
        logging.info("__CRON_EXCEPTION___", repr(e))
        print(repr(e))