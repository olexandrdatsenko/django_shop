from django.core.mail import send_mail
import time 
from celery import shared_task

@shared_task
def send_message_on_email(subject, message, from_email, recipient_list, html_message):
    send_mail(
        subject=subject,
        message=message,
        from_email=from_email,
        recipient_list=recipient_list,
        html_message=html_message,
    )
