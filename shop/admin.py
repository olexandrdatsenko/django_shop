from django.contrib import admin

from import_export import resources
from import_export.admin import ImportExportModelAdmin

from shop.models import Category, Product, Subcategory, Currency, Order


# Register your models here.
class ProductResource(resources.ModelResource):
    class Meta:
        model = Product
        exclude = ('id',)


class ProductAdmin(ImportExportModelAdmin):
    list_display = [field.name for field in Product._meta.fields]
    list_filter = ['title']  # поле для панели filter
    search_fields = ['title']  # add new field for search of title


admin.site.register((Category, Subcategory, Currency, Order))
admin.site.register(Product, ProductAdmin)
