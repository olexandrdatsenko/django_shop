from django.conf.urls import url

from .views import *
from .api import api

app_name = 'shop'
urlpatterns = [
    url(r'^$', IndexListView.as_view(), name='index'),
    url(r'^subcategory/(?P<pk_subcategory>\d+)/$', SubcategoryProductListView.as_view(),
        name='subcategory_product'),
    url(r'^subcategory/(?P<pk_product>\d+)/details/$', ProductDetailsView.as_view(),
        name='product_details'),
    url(r'^add-to-session/$', AddProductToSessionView.as_view(),
        name='add_to_session'),
    url(r'^remove-product-from-session/$', RemoveProductFromSessionView.as_view(),
        name='remove_from_session'),
    url(r'^remove-all-products-from-session/$', RemoveAllProductsFromSession.as_view(),
        name='remove_all_products'),
    url(r'^cart/$', CartView.as_view(), name='cart'),
    url(r'^order/$', MakeOrderView.as_view(), name='order'),
    url(r'^api/$', api),
]
