from django import forms
from django.forms import ModelForm, CharField

from .models import Order


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ["name_user", "phone_number", "email_user"]
        labels = {"name_user": ("Your name")}
        help_texts = {"name_user": ("Your full name")}
        error_messages = {"name_user": {"max_length": ("This writer's name is too long.")}}




