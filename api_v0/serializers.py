from rest_framework import serializers
from shop.models import Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ["title", "description", "amount", "price"]


class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ["title", "amount", "price"]
