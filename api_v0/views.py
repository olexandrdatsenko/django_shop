from rest_framework import viewsets

from shop.models import Product
from .serializers import ProductSerializer, ProductsSerializer


class ProductsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductsSerializer
